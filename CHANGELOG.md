## 0.8.0 (2025-01-11)

### :rocket: Features (2 changes)

- [:sparkles: Implement stale issues for GitLab](manoelpqueiroz/galactipy@6fc4776be0d77a5da2f2ab9530377e2dfa421cde) ([merge request](manoelpqueiroz/galactipy!34))
- [:sparkles: Implement autoreleases for template](manoelpqueiroz/galactipy@03488de7553a8e050784185aaa37f409b991e301) ([merge request](manoelpqueiroz/galactipy!34))

### :package: Build System & CI/CD (5 changes)

- [:green_heart: Fix incorrect default CI usage](manoelpqueiroz/galactipy@2d9435d9d3677bd5eb9929652ece41c1127f27b0) ([merge request](manoelpqueiroz/galactipy!34))
- [:green_heart: Fix project CI workflow usage](manoelpqueiroz/galactipy@a227f64c33c19da78b22ab1610579f3430782dd0) ([merge request](manoelpqueiroz/galactipy!33))
- [:construction_worker: Implement triage policies for project](manoelpqueiroz/galactipy@7c70af56fdf2909599e9c60cc56a819574f7f8cc) ([merge request](manoelpqueiroz/galactipy!34))
- [:construction_worker: Change project CI rules](manoelpqueiroz/galactipy@b7fc6fcf44c03eec39e9eee8fac06ac7a603e5c7) ([merge request](manoelpqueiroz/galactipy!33))
- [:green_heart: Fix Changelog commit job](manoelpqueiroz/galactipy@e932406337ad1433583541d475e35641a504012d) ([merge request](manoelpqueiroz/galactipy!32))

### :gear: Internals (1 change)

- [:safety_vest: Update post-gen hooks with triage policies removal](manoelpqueiroz/galactipy@1d8a56603fbd650276c0d092fef221e083dc3d19) ([merge request](manoelpqueiroz/galactipy!34))

### :construction_site: Template Internals (2 changes)

- [:construction_worker: Fix template CI rule enforcement](manoelpqueiroz/galactipy@0994bdc86e3ae13fd45f6bbe1db21f046bfab635)
- [:construction_worker: Refine template CI for GitLab](manoelpqueiroz/galactipy@8f3216ed73bbf859ae0e38e4fdb536a23ad6b6d2) ([merge request](manoelpqueiroz/galactipy!34))

### :memo: Documentation (4 changes)

- [:memo: Refine project roadmap](manoelpqueiroz/galactipy@7054e3df30d8e5ecc0247cf5487cc9277f1beb79) ([merge request](manoelpqueiroz/galactipy!33))
- [:wrench: Move instructions for project access token](manoelpqueiroz/galactipy@fcb98d59c251da47b49db681ca4b925c1acc463e) ([merge request](manoelpqueiroz/galactipy!34))
- [:memo: Replace TODOs section with project roadmap](manoelpqueiroz/galactipy@5da905bdce3cc82c481e524b67231da2ff51f9de) ([merge request](manoelpqueiroz/galactipy!32))
- [:memo: Add citation file for the project](manoelpqueiroz/galactipy@15a5b1576609e93fb87f53a75d90c5b9a674d546) ([merge request](manoelpqueiroz/galactipy!32))

## 0.7.0 (2025-01-09)

### :rocket: Features (6 changes)

- [:construction_worker: Refine GitLab PyPI upload CI](manoelpqueiroz/galactipy@651ce9a123c85b97650aec18c150c001794f0327) ([merge request](manoelpqueiroz/galactipy!31))
- [:truck: Separate test and production publishing for GitHub Actions](manoelpqueiroz/galactipy@a793f852b8aa50015a46b00450f9231e47ed21ac) ([merge request](manoelpqueiroz/galactipy!31))
- [:sparkles: Add PyPI publishing via GitHub Actions](manoelpqueiroz/galactipy@bbd8ad830e1fdcdf0e6bd133c886cfe2bbff7aa5) ([merge request](manoelpqueiroz/galactipy!31))
- [:sparkles: Add PyPI publishing via GitLab CI](manoelpqueiroz/galactipy@eb6c61fb70382384c210217b93913201a2533490) ([merge request](manoelpqueiroz/galactipy!31))
- [:children_crossing: Specify Poetry publishing exceptions](manoelpqueiroz/galactipy@aebca6f5a78f73fc70c255d939736410891725e8) ([merge request](manoelpqueiroz/galactipy!31))
- [:sparkles: Add PyPI publishing Invoke commands](manoelpqueiroz/galactipy@53649863f0dc7f4baf95f7a3b17be31eac2cad2b) ([merge request](manoelpqueiroz/galactipy!31))

### :wrench: Fixes & Refactoring (5 changes)

- [:pencil2: Fix Test PyPI configuration for Invoke](manoelpqueiroz/galactipy@f029c50be0e5df422075dd8d660ae0aa5edcbd94) ([merge request](manoelpqueiroz/galactipy!31))
- [:adhesive_bandage: Alter workflow files for optimal structure](manoelpqueiroz/galactipy@3f9682777146ec3da5a5a1a852666bcf22769a74) ([merge request](manoelpqueiroz/galactipy!31))
- [:adhesive_bandage: Fix typos in workflow files](manoelpqueiroz/galactipy@b3240351480d461d36b535bc1bd88fc5281a8401) ([merge request](manoelpqueiroz/galactipy!31))
- [:pencil2: Fix missing endraw directive for GitHub PyPI test](manoelpqueiroz/galactipy@bd227befda9fa31aac6fd48f56642409103250d5) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Ensure Poetry installation for template CI](manoelpqueiroz/galactipy@acdff9f9f82fc4eddfae10a6b7202009c18357f2) ([merge request](manoelpqueiroz/galactipy!31))

### :package: Build System & CI/CD (5 changes)

- [:construction_worker: Refine rules for automatic releases](manoelpqueiroz/galactipy@a6e594e9609269f703cbb4dab6ac1d6e13280da6) ([merge request](manoelpqueiroz/galactipy!30))
- [:construction_worker: Automatic releases for Galactipy](manoelpqueiroz/galactipy@dfcae1813337f104c0933d3e61fd5f81caea4ae8) ([merge request](manoelpqueiroz/galactipy!30))
- [:construction_worker: Add job for CHANGELOG commit for project](manoelpqueiroz/galactipy@d61768e39546b4c977ca4518347a8e903f4b7e36) ([merge request](manoelpqueiroz/galactipy!30))
- [:construction_worker: Set up release notes job for project](manoelpqueiroz/galactipy@25f12f6a51fe34b0177f958d359f1055ee78e0e0) ([merge request](manoelpqueiroz/galactipy!30))
- [:construction_worker: Configure CI to skip Work in Progress commits](manoelpqueiroz/galactipy@a8d85c730c2a31a268a6fc045cc0b64d2858ff13) ([merge request](manoelpqueiroz/galactipy!30))

### :gear: Internals (1 change)

- [:pushpin: Pin Poetry to v1.8.5](manoelpqueiroz/galactipy@b23e76c3fb3d1945fb766c1731e8d15c7ea972be) ([merge request](manoelpqueiroz/galactipy!30))

### :construction_site: Template Internals (13 changes)

- [:construction_worker: Change rule for when test workflow runs](manoelpqueiroz/galactipy@31de526f8936bd9590a8ed6859ef0999f9620ca4) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Refine rules GitHub workflows](manoelpqueiroz/galactipy@f1647003c53a43f868e6834754cc46f62321f98f) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Ensure PyPI test publishing for full releases](manoelpqueiroz/galactipy@ce8b54d15e09cc5beaec20af59b53bd9e2e923cb) ([merge request](manoelpqueiroz/galactipy!31))
- [:children_crossing: Add emoji to GitHub workflows and job steps](manoelpqueiroz/galactipy@c880f1fea206d6b5c7d8d08058bf2e658c8a5eb5) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Set up build and artifacts for PyPI uploads](manoelpqueiroz/galactipy@f29c212cd7fa12145020af60981eefeaa1d61b8b) ([merge request](manoelpqueiroz/galactipy!31))
- [:truck: Rename GitHub build workflow](manoelpqueiroz/galactipy@4cb8f6d6a5143ffcf6a48948284c32119ff30953) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Correct and simplify PyPI triggers for GitHub](manoelpqueiroz/galactipy@13929207c8ac34663df1b1f2e24c29d10c590b07) ([merge request](manoelpqueiroz/galactipy!31))
- [:children_crossing: Add link to GitHub Actions documentation](manoelpqueiroz/galactipy@357843ec0f58ffa37f647ff17c6cea3eef5f49be) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Streamline template GitLab CI](manoelpqueiroz/galactipy@cd22a3620eba9d44b069339f89a0df1f1347ffc8) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Move SemVer regex to pipeline variables](manoelpqueiroz/galactipy@8a4008e1d5978d8a8bb1ca4cc642d665c2e32652) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Set up build jobs for template](manoelpqueiroz/galactipy@181b343e13cd00d9eaa9d8190fdad49d59d08d8a) ([merge request](manoelpqueiroz/galactipy!31))
- [:construction_worker: Reorganize current template CI](manoelpqueiroz/galactipy@99da0478e4ce2e0ae3d68372846efadf7f29c804) ([merge request](manoelpqueiroz/galactipy!31))
- [:wrench: Fix template pyproject name keyword](manoelpqueiroz/galactipy@8914d84598a845a310d8c96aa8254148c497facc) ([merge request](manoelpqueiroz/galactipy!31))

### :memo: Documentation (3 changes)

- [:memo: Update READMEs with PyPI publishing](manoelpqueiroz/galactipy@6cf6299998cc335cb4485df65d51b755c6e13de5) ([merge request](manoelpqueiroz/galactipy!31))
- [:bulb: Correct TODOs marked as UPDATEMEs](manoelpqueiroz/galactipy@b665d0e647a74fbb35696aea03330316acddc4a8) ([merge request](manoelpqueiroz/galactipy!31))
- [:memo: Fill CHANGELOG.md with previous releases](manoelpqueiroz/galactipy@07ba1a9e4902660c7f8cff69e0103242ab35dfb6) ([merge request](manoelpqueiroz/galactipy!30))

### :arrow_up: Dependencies Updates (1 change)

- [:arrow_up: Upgrade lock file dependencies](manoelpqueiroz/galactipy@3fb6aac5ec5833294729e3b78e22837e877aa84b) ([merge request](manoelpqueiroz/galactipy!31))

## 0.6.1 (2024-12-03)

### :construction_site: Template Internals (1 change)

- [:wrench: Modify ignored SCM files for Todo Tree](manoelpqueiroz/galactipy@7b9d48d7afb7ab6ca6de37da0b834c56aa2b28dc) ([merge request](manoelpqueiroz/galactipy!29))

## 0.6.0 (2024-11-10)

### :rocket: Features (1 change)

- [:sparkles: Add VS Code settings](manoelpqueiroz/galactipy@f2e11303d4ef9be98d5571c0c0295036a987fe18) ([merge request](manoelpqueiroz/galactipy!28))

### :package: Build System & CI/CD (1 change)

- [:white_check_mark: Add tests for docstring length validation](manoelpqueiroz/galactipy@d374fb9921fb5c487b3bd89757f211d135628999) ([merge request](manoelpqueiroz/galactipy!28))

### :gear: Internals (3 changes)

- [:rotating_light: Fix linter warnings for hooks](manoelpqueiroz/galactipy@3718251fc8c5c95317e70b32949fa1ffaaabe0d2) ([merge request](manoelpqueiroz/galactipy!28))
- [:art: Add docstring line length validations to hooks](manoelpqueiroz/galactipy@90f71815dad8a042f353e46b4e6c98301442298e) ([merge request](manoelpqueiroz/galactipy!28))
- [:wrench: Update project Git trailers](manoelpqueiroz/galactipy@f5e4e702c83470811b68d4ca844a15b83997b8aa) ([merge request](manoelpqueiroz/galactipy!27))

### :memo: Documentation (2 changes)

- [:memo: Update READMEs with VS Code settings](manoelpqueiroz/galactipy@2a5344aadb0f6ec02d7b360de912cfff8436f2b5) ([merge request](manoelpqueiroz/galactipy!28))
- [:memo: Update project documentation with new Git trailers](manoelpqueiroz/galactipy@498c8ffb47b35842b5d8234f2ef45046a62c162c) ([merge request](manoelpqueiroz/galactipy!27))

### :construction_site: Template Internals (3 changes)

- [:wrench: Update template pyproject with preconfigured task-tags](manoelpqueiroz/galactipy@18050ffb62699785a0ff193d10358b516fb9772e) ([merge request](manoelpqueiroz/galactipy!28))
- [:wrench: Add Todo Tree configuration](manoelpqueiroz/galactipy@30c087d019da98dc4a7b668d4b1316d5a9c151ac) ([merge request](manoelpqueiroz/galactipy!28))
- [:wrench: Add further VS Code settings](manoelpqueiroz/galactipy@627dde8b986907ef32233644757da6b9f9fb54e7) ([merge request](manoelpqueiroz/galactipy!28))

## 0.5.0 (2024-11-03)

### :rocket: Features (2 changes)

- [:children_crossing: Add warning for missing Invoke installation](manoelpqueiroz/galactipy@2ebd93a6c72704a24e15ef185c59b7032a51d3ff) ([merge request](manoelpqueiroz/galactipy!25))
- [:children_crossing: Add Rich format to additional instructions](manoelpqueiroz/galactipy@f546b16327235f4c5afaee1ab34d25b6fd5f3cfc) ([merge request](manoelpqueiroz/galactipy!25))

### :wrench: Fixes & Refactoring (2 changes)

- [:adhesive_bandage: Disable emoji rendering for instructions](manoelpqueiroz/galactipy@227622d78108a5d86250acbea3618b6fb5a9ea47) ([merge request](manoelpqueiroz/galactipy!25))
- [:art: Reorganize Rich output](manoelpqueiroz/galactipy@2fa06b2761d158845bd7a0b185883c3366b944ed) ([merge request](manoelpqueiroz/galactipy!25))

### :package: Build System & CI/CD (2 changes)

- [:white_check_mark: Add tests for possible instruction outcomes](manoelpqueiroz/galactipy@20d17237184d8ba70a1b0350639ceddc43f13049) ([merge request](manoelpqueiroz/galactipy!25))
- [:white_check_mark: Fix additional instructions test](manoelpqueiroz/galactipy@72a073b2f255804a444befe409e41cf4eeffa3aa) ([merge request](manoelpqueiroz/galactipy!25))

### :memo: Documentation (1 change)

- [:memo: Add article on mocking to READMEs](manoelpqueiroz/galactipy@f4a167670baa93f25f02da55f5863d23ae2f58d1) ([merge request](manoelpqueiroz/galactipy!25))

### :arrow_up: Dependencies Updates (1 change)

- [:heavy_plus_sign: Add pytest-mock to project dependencies](manoelpqueiroz/galactipy@53cb90247b61b111b4f5a0649d96ee8ad7e2801a) ([merge request](manoelpqueiroz/galactipy!25))

## 0.4.1 (2024-11-03)

### :wrench: Fixes & Refactoring (1 change)

- [:wrench: Ensure removal of documentation variable](manoelpqueiroz/galactipy@f316bc71c65bed01122823c04f558e4a15537db4) ([merge request](manoelpqueiroz/galactipy!26))

### :package: Build System & CI/CD (2 changes)

- [:safety_vest: Remove documentation tasks from post-gen hooks](manoelpqueiroz/galactipy@2451bf918f9d7f059da71ffad62e9fcb93524ec0) ([merge request](manoelpqueiroz/galactipy!26))
- [:fire: Remove references to documentation variable](manoelpqueiroz/galactipy@1cca05779bbdb46a8b1d5070a5c365da26741ab9) ([merge request](manoelpqueiroz/galactipy!26))

## 0.4.0 (2024-11-03)

### :rocket: Features (2 changes)

- [:truck: Rename task for complete linting](manoelpqueiroz/galactipy@aef4b1a25e0a78b0b04a8eac724c3c709f2966b8) ([merge request](manoelpqueiroz/galactipy!24))
- [:heavy_plus_sign: Add Invoke to template dependencies](manoelpqueiroz/galactipy@b0cf8d763d79055e286246b3885771ee5a99ceb8) ([merge request](manoelpqueiroz/galactipy!24))

### :wrench: Fixes & Refactoring (3 changes)

- [:adhesive_bandage: Fix Docker Invoke commands](manoelpqueiroz/galactipy@132f1dd6b12af1e515724d7c2723e332d9a93a58) ([merge request](manoelpqueiroz/galactipy!24))
- [:pencil2: Fix color option variable in template](manoelpqueiroz/galactipy@5771bb8a56280f5cb5c27d5495fb5cd02c875420) ([merge request](manoelpqueiroz/galactipy!24))
- [:recycle: Change instructions to refer to Invoke](manoelpqueiroz/galactipy@e6c4b6057917e20a95a93ca65f54af34f917f67b) ([merge request](manoelpqueiroz/galactipy!24))

### :package: Build System & CI/CD (11 changes)

- [:wrench: Remove documentation variable from Cookiecutter configuration](manoelpqueiroz/galactipy@59c56f1a7f512f8627fd85266a26b3ff3b26780c) ([merge request](manoelpqueiroz/galactipy!24))
- [:rotating_light: Fix formatter warnings for project](manoelpqueiroz/galactipy@d836d57fe37b2b0e222000dc3accfcac13bf1cfc) ([merge request](manoelpqueiroz/galactipy!24))
- [:memo: Replace Makefile references with Invoke in issue templates](manoelpqueiroz/galactipy@371c21036fac1c5b7281fc97dc76df80db3995e4) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Include poetry-bumpversion configuration](manoelpqueiroz/galactipy@98d804d66d5d40358ad7d794f7bd0ec5bb920b49) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Optimise tasks with pyproject configuration](manoelpqueiroz/galactipy@b56f65c4366427fb6ba802ab4b0818fe46a3e4ba) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Update template CI files with Invoke](manoelpqueiroz/galactipy@a64bb5ae2c35436625889a7dcadf574b0eee2290) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Add multiple Docker images option](manoelpqueiroz/galactipy@1eb3d9bf59f36134e738157aed5d9ae608f0d05b) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Remove makefile keyword from configuration](manoelpqueiroz/galactipy@5165caef433378d61ab60801587d03f646b3fd8f) ([merge request](manoelpqueiroz/galactipy!24))
- [:fire: Remove template Makefile](manoelpqueiroz/galactipy@0e8d518f789fc3d6298045b0c9d262269acf6143) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Create Docker Invoke commands for template](manoelpqueiroz/galactipy@906c3923b8581a65b7a9a42f007fd638489142b9) ([merge request](manoelpqueiroz/galactipy!24))
- [:wrench: Mirror project mypy configuration in template](manoelpqueiroz/galactipy@edfa8e28eb3cd0306db83656c2dde57416e98a52) ([merge request](manoelpqueiroz/galactipy!24))

### :memo: Documentation (1 change)

- [:memo: Update READMEs with Invoke references](manoelpqueiroz/galactipy@703da3e026cb6e074349b7ddef93eb905a7f1f9a) ([merge request](manoelpqueiroz/galactipy!24))

## 0.3.2 (2024-11-02)

### :wrench: Fixes & Refactoring (4 changes)

- [:bug: Fix Python path for tasks](manoelpqueiroz/galactipy@6a7bd479a1aaed5481d2319a0008f57707f8b124) ([merge request](manoelpqueiroz/galactipy!23))
- [:rotating_light: Fix mypy errors](manoelpqueiroz/galactipy@2cf772c8a6dee61d0cd2086c5fe968a1298d7261) ([merge request](manoelpqueiroz/galactipy!23))
- [:adhesive_bandage: Change active venv path resolution](manoelpqueiroz/galactipy@ab6db1243dd12a7046d5f488a0df9f5dd910a009) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Fix mypy execution error](manoelpqueiroz/galactipy@7d65b9b04263af1af3fef6a2d1f5d53a17207459) ([merge request](manoelpqueiroz/galactipy!23))

### :package: Build System & CI/CD (17 changes)

- [:fire: Remove project Makefile](manoelpqueiroz/galactipy@6335cc8a0667c590e5191d557c02fdd94a802deb) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Add flag to ignore PTY in Poetry install](manoelpqueiroz/galactipy@035d6fa82ff872aea8126ff9211097706286177c) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Change pip placement in the pipeline](manoelpqueiroz/galactipy@76bf1ab87ec5234674495d4f6d6f01de1e7c27a8) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Update mypy configuration for the project](manoelpqueiroz/galactipy@2c1996f03bddda119cc216fb559f30e5712591cd) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Improve compilation task syntax](manoelpqueiroz/galactipy@e49b22594726de0c1bd5c8cf361f17635fcf3cf6) ([merge request](manoelpqueiroz/galactipy!23))
- [:white_check_mark: Fix Coverage exception with pytest-cookies](manoelpqueiroz/galactipy@ed7e6fe64a2adced2d5e8a817960f84ac6424829) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Add poetry-plugin-export explicitly to tasks](manoelpqueiroz/galactipy@24cd83a2a20a165634932f06ffbec8bb6d773459) ([merge request](manoelpqueiroz/galactipy!23))
- [:green_heart: Adhere to PEP 585 for annotating collections](manoelpqueiroz/galactipy@438cfb07e06c957bff9cd2578e87dc077abaf41b) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Add build directory removal command](manoelpqueiroz/galactipy@c5c2d698dfed7999532c649a15cec8f8a3be548c) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Create cleaning Invoke commands](manoelpqueiroz/galactipy@caac03c8fb5c6f191ce1de19f524b9120474428d) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Add dev dependencies upgrade command](manoelpqueiroz/galactipy@df48d7f54022f104148bbf18b6c1e85aaec82353) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Create linting Invoke commands](manoelpqueiroz/galactipy@c87c08f6533e47e09fec5869e8357a3279dc1ae4) ([merge request](manoelpqueiroz/galactipy!23))
- [:wastebasket: Remove Poetry installation check for Invoke tasks](manoelpqueiroz/galactipy@31dcb4882f4a0cdf26a27a62f94b9b3bfeb3f786) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Rework Invoke paths and environment variables](manoelpqueiroz/galactipy@71dddb5e1dc0e5890e5e3ab273809ace25ca09d4) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Create project installation Invoke commands](manoelpqueiroz/galactipy@56bbc905cc13b72e256100877eba94a22443d611) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Restructure Invoke PATH constants](manoelpqueiroz/galactipy@caede688f67f7dd4351f70a9d72756d3cd41a616) ([merge request](manoelpqueiroz/galactipy!23))
- [:wrench: Create Poetry installation Invoke commands](manoelpqueiroz/galactipy@31193a26b607ec8f5a99c7a63afd7e74ffbfa231) ([merge request](manoelpqueiroz/galactipy!23))

### :memo: Documentation (1 change)

- [:pencil2: Fix cleaning tasks docstrings](manoelpqueiroz/galactipy@d599c65380dd85e7b317743398a74e4a87b38cb8) ([merge request](manoelpqueiroz/galactipy!23))

### :arrow_up: Dependencies Updates (5 changes)

- [:arrow_up: Upgrade minimum development version to Python 3.10](manoelpqueiroz/galactipy@79ddf78515c0a7cdcef08279a6ae88ed5d09d5b2) ([merge request](manoelpqueiroz/galactipy!23))
- [:arrow_down: Downgrade Safety to avoid CI failure](manoelpqueiroz/galactipy@53cadcd89880a315e26242f3c0b16dff3dfebb20) ([merge request](manoelpqueiroz/galactipy!23))
- [:arrow_up: Upgrade dev dependencies to latest versions](manoelpqueiroz/galactipy@3ab5b755314899e8632b153d19b5805ba0282a83) ([merge request](manoelpqueiroz/galactipy!23))
- [:arrow_up: Deprecate Python 3.8 and add Python 3.13](manoelpqueiroz/galactipy@4f9713a90a45da80f18ae96f2a065d09a6769745) ([merge request](manoelpqueiroz/galactipy!23))
- [:heavy_plus_sign: Add Invoke to project dependencies](manoelpqueiroz/galactipy@6361f6cef6a707f58a0cb2112823ef74e9a8ebff) ([merge request](manoelpqueiroz/galactipy!23))

## 0.3.1 (2024-07-13)

### :wrench: Fixes & Refactoring (2 changes)

- [:bug: Fix partial module initialisation](manoelpqueiroz/galactipy@61ac4e193a336f6dc4c73ca6800ea2389b629de3) ([merge request](manoelpqueiroz/galactipy!17))
- [:wrench: Update Cookiecutter configuration behaviour](manoelpqueiroz/galactipy@35f43a0a67ec2464f43081f77bd76144c8160d06) ([merge request](manoelpqueiroz/galactipy!17))

## 0.3.0 (2024-07-13)

### :wrench: Fixes & Refactoring (18 changes)

- [:rotating_light: Select flake8-boolean-trap rules for template](manoelpqueiroz/galactipy@d133935368ec53d3a52820c725cd52f0b4b4ab13) ([merge request](manoelpqueiroz/galactipy!19))
- [:rotating_light: Select flake8-todos rules for template](manoelpqueiroz/galactipy@7e34353e4cc30d642bc7363f99ab3575ea2afe35) ([merge request](manoelpqueiroz/galactipy!19))
- [:rotating_light: Select additional rules for template](manoelpqueiroz/galactipy@9ea7767fd8984b1dcde75efdc8cf65ab2867c548) ([merge request](manoelpqueiroz/galactipy!19))
- [:rotating_light: Fix missing character in noqa inline comments](manoelpqueiroz/galactipy@afb237b8a092f6e319fab658b92de79f7259c62b) ([merge request](manoelpqueiroz/galactipy!19))
- [:rotating_light: Fix minor linter warnings for template](manoelpqueiroz/galactipy@37cf6404aae28db2a39b0d758987cad9a4fecc3c) ([merge request](manoelpqueiroz/galactipy!19))
- [:rotating_light: Select flake8-pytest-style rules for project](manoelpqueiroz/galactipy@b84fe821766a86a0ee4949d69c9dfd768b321128) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select flake8-unused-arguments rules for project](manoelpqueiroz/galactipy@5f1d8b3a83625836803b27e3b1c3b3a1b1e7c86a) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select Pylint rules for project](manoelpqueiroz/galactipy@40fc0d511274270ea93fd245cb1629acf9f2ffe0) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select flake8-raise rules for project](manoelpqueiroz/galactipy@201a195ed48a2baf8bea451868e0b130b39331e3) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select flake8-bugbear rules for project](manoelpqueiroz/galactipy@996604a1df362def988cf27465352754d490816f) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select additional rules for project](manoelpqueiroz/galactipy@fd977ef6ff6bb37bfbd745d4e66349b76508fbe3) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select flake8-bandit rules for project](manoelpqueiroz/galactipy@f0704fefb5b86cf9f2d40f300ce04c6778dc3883) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select pep8-annotations for project](manoelpqueiroz/galactipy@18b32d3dfcc93ae41ee02f92126a37d27c65bc69) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select pydocstyle rules for project](manoelpqueiroz/galactipy@bfc23ce13dab9c7ca3b875bf33618585488ca2bd) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select pep8-naming rules for project](manoelpqueiroz/galactipy@6d6b81a9d3fb77f1038057a68d256c7ae2642928) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select isort rules for project](manoelpqueiroz/galactipy@a960f8e953a92d3867f4e1cb2d82a65e2ce2b742) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select pycodestyle rules for project](manoelpqueiroz/galactipy@01fa2bad647549c0b49deca186c519247d2a6a72) ([merge request](manoelpqueiroz/galactipy!18))
- [:rotating_light: Select Pyflakes rules for project](manoelpqueiroz/galactipy@314833ef913ca1a08336746ecd5fcf572ad7f8e1) ([merge request](manoelpqueiroz/galactipy!18))

### :package: Build System & CI/CD (4 changes)

- [:green_heart: Ignore jinja2 Safety warning](manoelpqueiroz/galactipy@1efe5d4105afff46a4a2eaad96625497390685e4) ([merge request](manoelpqueiroz/galactipy!19))
- [:green_heart: Replace check-codestyle with check-linter](manoelpqueiroz/galactipy@758e79b18dcad116a3efe5cd99bf543bada115e1) ([merge request](manoelpqueiroz/galactipy!19))
- [:wrench: Update project and template Makefiles](manoelpqueiroz/galactipy@c7a697469b1d6971a6e411168e1627a7e5fdc8bb) ([merge request](manoelpqueiroz/galactipy!19))
- [:wrench: Include additional ruff configuration](manoelpqueiroz/galactipy@09c15c60b0c76d4f6e1f5967421a1ad286525d2b) ([merge request](manoelpqueiroz/galactipy!18))

### :memo: Documentation (4 changes)

- [:memo: Update non-OSS sections in README](manoelpqueiroz/galactipy@571d5a31476c2a337fbb7db9a341f9fb0a7a9650) ([merge request](manoelpqueiroz/galactipy!19))
- [:memo: Update minor issues in project README](manoelpqueiroz/galactipy@de9a363cfda13854cc20d35d3d6cf2c7fb6bb44b) ([merge request](manoelpqueiroz/galactipy!19))
- [:memo: Update Makefile section in READMEs](manoelpqueiroz/galactipy@e99c1f70dd45c4f333a01a996d6fad704ab9dcf2) ([merge request](manoelpqueiroz/galactipy!19))
- [:memo: Update READMEs with references to Ruff](manoelpqueiroz/galactipy@5ba0be4cbd5d74080e8a1d7b6a142d2304b850f7) ([merge request](manoelpqueiroz/galactipy!19))

### :arrow_up: Dependencies Updates (4 changes)

- [:heavy_plus_sign: Update dependencies](manoelpqueiroz/galactipy@7fea53bf0614cf07ed6fc5cd2f2912dde0568c3c) ([merge request](manoelpqueiroz/galactipy!19))
- [:wrench: Implement ruff for template](manoelpqueiroz/galactipy@b01524e6cc68a72d226a9f59936760ebb085fbb8) ([merge request](manoelpqueiroz/galactipy!19))
- [:heavy_minus_sign: Remove old linters and hooks](manoelpqueiroz/galactipy@68442e3a7f8a490fabb40a5e836172df413c639a) ([merge request](manoelpqueiroz/galactipy!18))
- [:heavy_plus_sign: Add ruff to project dependencies](manoelpqueiroz/galactipy@8840c55c33aa3ad3399f1327c016681a6ccd8465) ([merge request](manoelpqueiroz/galactipy!18))

## 0.2.3 (2024-05-05)

### :wrench: Fixes & Refactoring (2 changes)

- [:adhesive_bandage: Correct Poetry licence for non-OSS option](manoelpqueiroz/galactipy@916c3e7b1b82b37aad7bd8139649bce78a2da8f3) ([merge request](manoelpqueiroz/galactipy!16))
- [:bug: Fix behaviour for file removals](manoelpqueiroz/galactipy@816ef86397f0e9d6ff91b038dbbc778972875495) ([merge request](manoelpqueiroz/galactipy!16))

### :arrow_up: Dependencies Updates (1 change)

- [:arrow_up: Add support for Python 3.12](manoelpqueiroz/galactipy@a02966c73d60263f3b49c79dc4c004cf3b5e247c) ([merge request](manoelpqueiroz/galactipy!16))

## 0.2.2 (2024-05-05)

### :wrench: Fixes & Refactoring (4 changes)

- [:ambulance: Fix PackageNotFoundError import](manoelpqueiroz/galactipy@34a550d80667dcba94582c8b06fecab0d413799e) ([merge request](manoelpqueiroz/galactipy!15))
- [:wrench: Remove default_language_version from pre-commit](manoelpqueiroz/galactipy@52582adf2fa418f08336ebd66700c3103949a0f7) ([merge request](manoelpqueiroz/galactipy!15))
- [:wrench: Update Cookiecutter configuration](manoelpqueiroz/galactipy@f89dca3db967a370633446bdd120732b91fc890b) ([merge request](manoelpqueiroz/galactipy!15))
- [:art: Provide hello function at top-level module](manoelpqueiroz/galactipy@f402d2fbfe700a449ab685a4c60f9442375e05ec) ([merge request](manoelpqueiroz/galactipy!15))

### :package: Build System & CI/CD (1 change)

- [:green_heart: Ensure pip update to latest version](manoelpqueiroz/galactipy@df68aaa29d3b28ec3165a90e4e70834e4bd718c1) ([merge request](manoelpqueiroz/galactipy!15))

### :arrow_up: Dependencies updates (1 change)

- [:arrow_up: Update Black for safety check](manoelpqueiroz/galactipy@602aa7bba82cc33884ffe5e4f6f85e867c59beb5) ([merge request](manoelpqueiroz/galactipy!15))

## 0.2.1 (2023-11-19)

### :wrench: Fixes & Refactoring (1 change)

- [:ambulance: Fix version retrieval code](manoelpqueiroz/galactipy@bf6ae87f1aed2c48cec4800767eac7ee5a528745) ([merge request](manoelpqueiroz/galactipy!13))

### :package: Build System & CI/CD (2 changes)

- [:white_check_mark: Modify benchmark test for template](manoelpqueiroz/galactipy@3dc0c60373d0cdadbea6a945e38570d67bb6d35b) ([merge request](manoelpqueiroz/galactipy!13))
- [:green_heart: Mark template test for skip](manoelpqueiroz/galactipy@609431220cfaa897a9a04abd3672e5c5bb9c7fd2) ([merge request](manoelpqueiroz/galactipy!13))

## 0.2.0 (2023-11-19)

### :wrench: Fixes & Refactoring (6 changes)

- [:bug: Fix example color output](manoelpqueiroz/galactipy@c2ca18ff5340b5f2d8d768e2ee49e5f8811c86a1) ([merge request](manoelpqueiroz/galactipy!12))
- [:adhesive_bandage: Update importlib_metadata code](manoelpqueiroz/galactipy@87ef013c40e8c4dc23dc472af6d37150491d6bd2) ([merge request](manoelpqueiroz/galactipy!12))
- [:ambulance: Fix Poetry reference for package install](manoelpqueiroz/galactipy@e6cb711913b23085fa990f854c7877d6d66f835f) ([merge request](manoelpqueiroz/galactipy!8))
- [:wrench: Replicate coverage config to template files](manoelpqueiroz/galactipy@ad58783dce435f4ccc7a3f4105b1f1ebaa329797) ([merge request](manoelpqueiroz/galactipy!4))
- [:wrench: Update TOML with coverage rules](manoelpqueiroz/galactipy@d4968afedcf2444b1dbf5033377c18e9e5cb9a26) ([merge request](manoelpqueiroz/galactipy!4))
- [:adhesive_bandage: Convert tests directory to package](manoelpqueiroz/galactipy@ba31baa55a6f4612e11c5d42405d1d752e58adde) ([merge request](manoelpqueiroz/galactipy!3))

### :package: Build System & CI/CD (19 changes)

- [:construction_worker: Change CI workflow rules](manoelpqueiroz/galactipy@165889db3e65be4fde403457aecf52ed8f736723) ([merge request](manoelpqueiroz/galactipy!11))
- [:white_check_mark: Add test for template generation](manoelpqueiroz/galactipy@de2acecd6b6658d95f5c0bfcb4f217e83f9b5226) ([merge request](manoelpqueiroz/galactipy!9))
- [:white_check_mark: Add further tests to increase coverage](manoelpqueiroz/galactipy@c9dba3b9a5b2bcfa77a1c3ca395e3556f9a697cf) ([merge request](manoelpqueiroz/galactipy!4))
- [:construction_worker: Generate coverage report artifact for GitLab](manoelpqueiroz/galactipy@231ef3b70cd3928ec976ffbd5c9d1ea4f2518033) ([merge request](manoelpqueiroz/galactipy!3))
- [:construction_worker: Enable Coveralls upload for project](manoelpqueiroz/galactipy@1bbfd35d5d143e50c2a5a7f13d8d33e3e548c5cf) ([merge request](manoelpqueiroz/galactipy!3))
- [:white_check_mark: Add tests for unused files removal](manoelpqueiroz/galactipy@7561df54049f208be7e7cf6c385924cedb3cf551) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add non-OSS licence test](manoelpqueiroz/galactipy@b7b639acf61b4f27d73d74c76ceac1123b577b24) ([merge request](manoelpqueiroz/galactipy!1))
- [:truck: Organise fixture for licence generation](manoelpqueiroz/galactipy@a5bc8521d3cfee97838d46894ba9b6afac1ee21a) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add tests for template generation](manoelpqueiroz/galactipy@a9a9ff64190324a797ff38b78b98c7d482de9632) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add tests for licence generation](manoelpqueiroz/galactipy@048953aaecd3efa6ba6185a9ea9da6c2bb6e4fda) ([merge request](manoelpqueiroz/galactipy!1))
- [:test_tube: Include tests for protected file/directory](manoelpqueiroz/galactipy@867395c3582287e81e4aeeab13629952fed067e5) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Create class tests for additional operations](manoelpqueiroz/galactipy@37935d55432fbd32b9c626f10343b760317cc107) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add class test for file/directory operations](manoelpqueiroz/galactipy@f2939ac007494697c17b9f47d4bf24514f250236) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add semver unit tests](manoelpqueiroz/galactipy@a055e5f8368e3b08ae87840324dee79d0608fb6a) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add username unit tests](manoelpqueiroz/galactipy@2403d426afc74bce9a89c8d6f0623ae91688422b) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add package name unit tests](manoelpqueiroz/galactipy@3df090830b6f00f7d518273c5b64e135f3987f92) ([merge request](manoelpqueiroz/galactipy!1))
- [:pencil2: Include raw string reference for reserved names](manoelpqueiroz/galactipy@16dc642a73c657306aa399d79d513d5cfcf188eb) ([merge request](manoelpqueiroz/galactipy!1))
- [:white_check_mark: Add repo name unit tests](manoelpqueiroz/galactipy@4434e0b24feed8e089823b43138f74544f76c127) ([merge request](manoelpqueiroz/galactipy!1))
- [:fire: Adjust project slug regex](manoelpqueiroz/galactipy@e920fb43d9f4a64c861a3cc0a484a7a9dfa567f7) ([merge request](manoelpqueiroz/galactipy!1))

### :memo: Documentation (2 changes)

- [:memo: Update READMEs with information on testing](manoelpqueiroz/galactipy@52c1f3e9fcdcf170a040f3a702b9dec9be8d3a8e) ([merge request](manoelpqueiroz/galactipy!6))
- [:memo: Update CONTRIBUTING with GitHub mirror](manoelpqueiroz/galactipy@898fc928dd59378a3fb9c31de846b51f5bc19673)

### :arrow_up: Dependencies Updates (8 changes)

- [:heavy_plus_sign: Add pytest-benchmark to template dependencies](manoelpqueiroz/galactipy@ee39ce7d394088a9f15af991b976ef64f28d887d) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_plus_sign: Add pytest-click to template dependencies](manoelpqueiroz/galactipy@08cf00dbc19224fd6bc696756d8c5cee86b7f4f9) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_plus_sign: Add pytest-pikachu to template dependencies](manoelpqueiroz/galactipy@ec0f0a562c7775a4390478857001043d12c0000b) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_plus_sign: Add pytest-sugar to project and template](manoelpqueiroz/galactipy@dce4ced7f96b9232616a28ad93f7923aac466f72) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_minus_sign: Remove pytest-html from project dependencies](manoelpqueiroz/galactipy@b13f2197f19c9df8ab06ed2e47ecdc94b32c07af) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_plus_sign: Add pytest-timeout to project and template](manoelpqueiroz/galactipy@8e2e0990dac823822fdebe13687514df9644fe39) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_plus_sign: Add pytest-mock to template dependencies](manoelpqueiroz/galactipy@31fb5c4f18a688bf314dd29c855546a3b6d781b5) ([merge request](manoelpqueiroz/galactipy!6))
- [:heavy_minus_sign: Remove coverage-badge from dependencies](manoelpqueiroz/galactipy@99a5d77b2dbd5f9dda22a3a1988df45f55750b49) ([merge request](manoelpqueiroz/galactipy!6))

## 0.1.0 (2023-10-04)

Initial functional release of `galactipy`.

### :rocket: Features

- Option to select between GitLab and GitHub as platforms
- Seven different licences to choose from, plus an option to generate the project as closed source
- Options to use formatters and linters, with further configuration of line length and docstring style
- Options to create Docker image and documentation structure

### :package: Build System & CI/CD

- Extended pre-gen hooks to validate repository name and username
- Modified post-gen hooks to remove unnecessary files considering current Cookiecutter options
- GitLab CI configuration mirroring GitHub build to check for codestyle, testing and safety
- Use of `poetry-plugin-up` for updating dev dependencies via `Makefile` function

### :memo: Documentation

- Update to Contributor Covenant v2.1
- Extended `README` guide with details on how to further set up the project
- Included additional reading suggestions to `README`

### :arrow_up: Dependencies Updates

- Support from `Python 3.8` onwards
- Default dev dependencies set at earliest versions compatible with `Python 3.8` through `3.11` (except for `flake8`)
